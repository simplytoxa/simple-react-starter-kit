"use strict";

import React from 'react';
import {render} from 'react-dom';

export class App extends React.Component {
  render() {
    return(
      <h1>It works!!!</h1>
    );
  }
}

render(<App />, document.getElementById('container'));